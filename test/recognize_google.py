import speech_recognition as sr

r = sr.Recognizer()

hello_audio = sr.AudioFile('voice.wav')
with hello_audio as source:
    audio = r.record(source)

print(r.recognize_google(audio, show_all=True))
