import argparse
import json
import os
from os import path
import queue
import sys
import time

import sounddevice as sd
import vosk

from fuzzywuzzy import fuzz


def fuzzy_matching(words, targets, threshold):
    # print(words, targets)
    if not targets:
        # if targets is empty -> match!
        return True, words

    if targets and not words:
        return False, words

    # here targets and words are not empty!
    for word in words:
        if fuzz.ratio(word, targets[0]) > threshold:
            return fuzzy_matching(words[1:], targets[1:], threshold)
        else:
            return fuzzy_matching(words[1:], targets, threshold)


def main():
    q = queue.Queue()

    def int_or_str(text):
        """Helper function for argument parsing."""
        try:
            return int(text)
        except ValueError:
            return text

    def callback(indata, frames, time, status):
        """This is called (from a separate thread) for each audio block."""
        if status:
            print(status, file=sys.stderr)
        q.put(bytes(indata))

    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument(
        '-l', '--list-devices', action='store_true',
        help='show list of audio devices and exit')
    args, remaining = parser.parse_known_args()
    if args.list_devices:
        print(sd.query_devices())
        parser.exit(0)

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        parents=[parser])
    parser.add_argument(
        '-f', '--filename', type=str, metavar='FILENAME',
        help='audio file to store recording to')
    parser.add_argument(
        '-m', '--model', type=str, metavar='MODEL_PATH',
        help='Path to the model')
    parser.add_argument(
        '-d', '--device', type=int_or_str,
        help='input device (numeric ID or substring)')
    parser.add_argument(
        '-r', '--samplerate', type=int, help='sampling rate')
    args = parser.parse_args(remaining)

    last_activation_time = None

    try:
        if args.model is None:
            model_path = path.join(path.dirname(path.abspath(__file__)),
                                   "..", "vosk-models", "vosk-model-small-en-us-0.15")
            args.model = model_path
        if not os.path.exists(args.model):
            print("Please download a model for your language from https://alphacephei.com/vosk/models")
            print("and unpack as 'model' in the current folder.")
            parser.exit(0)
        if args.samplerate is None:
            device_info = sd.query_devices(args.device, 'input')
            # soundfile expects an int, sounddevice provides a float:
            args.samplerate = int(device_info['default_samplerate'])

        model = vosk.Model(args.model)

        if args.filename:
            dump_fn = open(args.filename, "wb")
        else:
            dump_fn = None

        # does it start a separate thread to read from a microphone?
        with sd.RawInputStream(samplerate=args.samplerate, blocksize=8000, device=args.device, dtype='int16',
                               channels=1, callback=callback):
            print('#' * 80)
            print('Press Ctrl+C to stop the recording')
            print('#' * 80)

            rec = vosk.KaldiRecognizer(model, args.samplerate)
            while True:
                data = q.get()
                if rec.AcceptWaveform(data):
                    ret = json.loads(rec.Result())
                    print('result:', ret)

                    transcript = ret['text']
                    words = transcript.split()

                    if last_activation_time:
                        # it's been activated
                        # search for a specific command
                        f, _ = fuzzy_matching(words, ['start', 'sleep', 'session'], 80)
                        if f:
                            print('[voice command recognized] start sleep session')
                            continue

                        f, _ = fuzzy_matching(words, ['start', 'focus', 'session'], 80)
                        if f:
                            print('[voice command recognized] start focus session')
                            continue

                        f, _ = fuzzy_matching(words, ['give', 'overview'], 80)
                        if f:
                            print('[voice command recognized] overview')
                            continue

                        f, _ = fuzzy_matching(words, ['play', 'different', 'song'], 80)
                        if f:
                            print('[voice command recognized] play a different song')
                            continue

                        f, remains = fuzzy_matching(words, ['extend', 'sleep', 'session'], 80)
                        if f:
                            print('[void command recognized] extend sleep session', remains)
                            continue

                        f, remains = fuzzy_matching(words, ['extend', 'sleep', 'session'], 80)
                        if f:
                            print('[voice command recognized] extend focus session', remains)
                            continue

                    elif fuzz.ratio(transcript, 'hey there') > 80:
                        last_activation_time = time.time()
                        print('activated!')
                # else:
                #     print(rec.PartialResult())

                if last_activation_time and last_activation_time < time.time() - 4:
                    last_activation_time = None
                    print('deactivate')

                if dump_fn is not None:
                    dump_fn.write(data)
    except KeyboardInterrupt:
        print('\nDone')
        parser.exit(0)
    except Exception as e:
        print(type(e).__name__ + ': ' + str(e))
        parser.exit(-1)


if __name__ == '__main__':
    main()
