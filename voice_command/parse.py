from enum import Enum, auto
import datetime
import re


def text2int(words):
    """
    convert numbers in English to int

    e.g. "two hundreds and thirty four" -> 234
    Args:
         words ([list[str]])

    Returns:
        int
    """
    units = [
        "zero", "one", "two", "three", "four", "five", "six", "seven", "eight",
        "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
        "sixteen", "seventeen", "eighteen", "nineteen",
    ]

    tens = ["", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"]

    scales = ["hundred", "thousand", "million", "billion", "trillion"]

    numwords = {"and": (1, 0)}
    for idx, word in enumerate(units):
        numwords[word] = (1, idx)
    for idx, word in enumerate(tens):
        numwords[word] = (1, idx * 10)
    for idx, word in enumerate(scales):
        numwords[word] = (10 ** (idx * 3 or 2), 0)
        numwords[word + 's'] = (10 ** (idx * 3 or 2), 0)

    current = result = 0
    for word in words:
        if word not in numwords:
            raise ValueError("Illegal word: " + word)

        scale, increment = numwords[word]
        current = current * scale + increment
        if scale > 100:
            result += current
            current = 0

    return result + current


class CommandParserState(Enum):
    INIT = auto()
    START_ACCEPTED = auto()
    START_SLEEP_ACCEPTED = auto()
    START_FOCUS_ACCEPTED = auto()

    EXTEND_ACCEPTED = auto()
    EXTEND_SLEEP_ACCEPTED = auto()
    EXTEND_FOCUS_ACCEPTED = auto()

    SET_ACCEPTED = auto()
    SET_ALARM_ACCEPTED = auto()

    TELL_ACCEPTED = auto()
    TELL_HEALTH_ACCEPTED = auto()
    TELL_HEALTH_STATISTICS_ACCEPTED = auto()

    FAILED = auto()


def take_command(words, now=None):
    state = CommandParserState.INIT
    if now is None:
        now = datetime.datetime.now()

    initiative_words = ['start', 'begin', 'extend', 'set', 'create', 'make', 'show', 'tell']

    while words:
        word = words.pop(0).lower()

        # -->
        if state == CommandParserState.INIT:
            if word in ['start', 'begin']:
                state = CommandParserState.START_ACCEPTED
            elif word in ['extend', 'lengthen']:
                state = CommandParserState.EXTEND_ACCEPTED
            elif word in ['set', 'create', 'make']:
                state = CommandParserState.SET_ACCEPTED
            elif word in ['show', 'tell']:
                state = CommandParserState.TELL_ACCEPTED

        # START -->
        elif state == CommandParserState.START_ACCEPTED:
            if word == 'sleep':
                state = CommandParserState.START_SLEEP_ACCEPTED
                # return state
            elif word == 'focus':
                state = CommandParserState.START_FOCUS_ACCEPTED
                # return state
            elif word in initiative_words:
                words.insert(0, word)
                state = CommandParserState.INIT

        # START --> SLEEP -->
        elif state == CommandParserState.START_SLEEP_ACCEPTED:
            if word in ['session', 'study']:
                return state, None
            elif word == 'focus':
                state = CommandParserState.START_FOCUS_ACCEPTED
            elif word in initiative_words:
                state = CommandParserState.INIT
                words.insert(0, word)

        # START --> FOCUS -->
        elif state == CommandParserState.START_FOCUS_ACCEPTED:
            if word == ['session', 'study']:
                return state, None
            elif word == 'sleep':
                state = CommandParserState.START_SLEEP_ACCEPTED
            elif word in initiative_words:
                state = CommandParserState.INIT
                words.insert(0, word)

        # EXTEND -->
        elif state == CommandParserState.EXTEND_ACCEPTED:
            if word == 'sleep':
                state = CommandParserState.EXTEND_SLEEP_ACCEPTED
            elif word == 'focus':
                state = CommandParserState.EXTEND_FOCUS_ACCEPTED
            elif word in initiative_words:
                words.insert(0, word)
                state = CommandParserState.INIT

        # EXTEND --> SLEEP -->
        elif state == CommandParserState.EXTEND_SLEEP_ACCEPTED:
            if word == 'session':
                ret, r = take_prop_duration(words, now)
                if ret:
                    return state, ret
                else:
                    return None, r
            elif word == 'focus':
                state = CommandParserState.EXTEND_FOCUS_ACCEPTED
            elif word in initiative_words:
                words.insert(0, word)
                state = CommandParserState.INIT

        # EXTEND --> FOCUS -->
        elif state == CommandParserState.EXTEND_FOCUS_ACCEPTED:
            if word in ['session', 'study']:
                ret, r = take_prop_duration(words, now)
                if ret:
                    return state, ret
                else:
                    return None, r
            elif word.startswith('sleep'):
                state = CommandParserState.EXTEND_SLEEP_ACCEPTED
            elif words in initiative_words:
                words.insert(0, word)
                state = CommandParserState.INIT

        # SET -->
        elif state == CommandParserState.SET_ACCEPTED:
            if word == 'alarm':
                # SET --> ALARM
                state = CommandParserState.SET_ALARM_ACCEPTED

                # case 1: set alarm (time) (am|pm)?
                t, _ = take_time(words, now)

                # case 2: set alarm at (time) (am|pm)?
                if t is None:
                    t, _ = take_prop_time(words, 'at', now)

                # case 3: set alarm (for|in) (duration) from now
                if t is None:
                    duration_from_now, _ = take_for_duration_from_now(words)

                    if duration_from_now is not None:
                        t = now + duration_from_now

                # case 4: set an alarm for (time)
                if t is None:
                    t, _ = take_prop_time(words, 'for', now)

                # if any one of these matches
                if t is not None:
                    if t < now:
                        t += datetime.timedelta(0, 12 * 3600)

                    return state, t
                else:
                    return CommandParserState.FAILED, None
        elif state == CommandParserState.TELL_ACCEPTED:
            if word == 'health':
                state = CommandParserState.TELL_HEALTH_ACCEPTED
            elif word == 'dashboard':
                state = CommandParserState.TELL_HEALTH_STATISTICS_ACCEPTED
                return state, None
        elif state == CommandParserState.TELL_HEALTH_ACCEPTED:
            if word == 'statistics':
                state = CommandParserState.TELL_HEALTH_STATISTICS_ACCEPTED

                return state, None

    if state == CommandParserState.START_SLEEP_ACCEPTED:
        return state, None
    elif state == CommandParserState.START_FOCUS_ACCEPTED:
        return state, None

    return CommandParserState.FAILED, None


def take_num_words(words):
    """
    e.g.

    ['two', 'hundreds', 'and', 'fifteen']
    ['seventy', 'five']
    """
    num_words = []
    remain_words = list(words)
    num = None

    while remain_words:
        num_words.append(remain_words[0])
        try:
            num = text2int(num_words)
        except ValueError:
            if num is None:
                return None, words
            break
        remain_words.pop(0)

    return num, remain_words


def take_time(words, now=None):
    """
    take a time from the beginning of words
    it searches the nearest future time specified by words


    input examples:
    ['six', 'thirty']
    ['five', 'twenty', 'three', 'am']
    ['seven' 'am']

    ['7' 'a.m.']
    ['6:30' 'p.m.']

    Args:
        words (List[str]):
        now (datetime.datetime):

    Returns:
        (datetime.datetime | None, List[str]): time and the remainder of words
    """
    if len(words) < 1:
        return None, words

    def get_time_depends_on_now(h, m, n):
        """
        when a.m. or p.m are not specified
        """

        t = n.replace(hour=h, minute=m, second=0, microsecond=0)
        if t < n:
            t += datetime.timedelta(0, 12 * 3600)
        if t < n:
            t += datetime.timedelta(0, 12 * 3600)

        return t

    if now is None:
        now = datetime.datetime.now().replace(second=0, microsecond=0)

    # case 1: '7'
    # convert it into a '7:00' form
    # it assumes that the transcription API gives time in xx:xx format if minute is specified
    m = re.match(r'^\d+$', words[0])
    if m:
        words[0] = words[0] + ":00"

    # case 2: '6:30'
    m = re.match(r'^(\d+):(\d+)$', words[0])
    if m:
        hour = int(m.group(1))
        minute = int(m.group(2))

        if len(words) < 2:
            return get_time_depends_on_now(hour, minute, now), words[1:]
        else:
            if words[1] in ['am', 'a.m.']:
                t, remain_words = now.replace(hour=hour, minute=minute), words[2:]
                if t < now:
                    t += datetime.timedelta(1)
                return t, remain_words
            elif words[1] in ['pm', 'p.m.']:
                t, remain_words = now.replace(hour=hour if hour >= 12 else hour + 12, minute=minute), words[2:]
                if t < now:
                    t += datetime.timedelta(1)
                return t, remain_words
            else:
                return get_time_depends_on_now(hour, minute, now), words[1:]

    hour_nums = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve']

    # case 3
    if words[0] not in hour_nums:
        return None, words
    else:
        hour = text2int(words[:1])
        remain_words = words[1:]

        if len(words) < 2:
            return get_time_depends_on_now(hour, 0, now), remain_words
        elif words[1] in ['am', 'a.m.']:
            return now.replace(hour=hour, minute=0), remain_words[1:]
        elif words[1] in ['pm', 'p.m.']:
            t, remain_words = now.replace(hour=hour if hour >= 12 else hour + 12, minute=0), remain_words[1:]
            if t < now:
                t += datetime.timedelta(1, )
            return t, remain_words
        else:
            # minute number specified?
            minute, remain_words = take_num_words(remain_words)

            if minute is None:
                return get_time_depends_on_now(hour, 0, now), remain_words
            elif remain_words and remain_words[0] in ['a.m', 'am']:
                t, remain_words = now.replace(hour=hour, minute=minute), remain_words[1:]
                if t < now:
                    t += datetime.timedelta(1)
                return t, remain_words
            elif remain_words and remain_words[0] in ['p.m.', 'pm']:
                t, remain_words = now.replace(hour=hour if hour >= 12 else hour + 12, minute=minute), remain_words[1:]
                if t < now:
                    t += datetime.timedelta(1)
                return t, remain_words
            else:
                # 'a.m.' or 'p.m.' not specified
                return get_time_depends_on_now(hour, minute, now), remain_words


def take_prop_time(words, prop, now=None):
    """
    expected input

    (specified prop) (time)

    e.g.
    ['at', 'seven', 'a.m.']
    ['for', '7:30', 'p.m.']

    Args:
        words (List[str])
        prop (str): 'for', 'at', or something else
        now (datetime.datetime): reference time

    Returns:
        (datetime.datetime | None, remainder of words)
    """
    remain_words = list(words)

    while remain_words:
        word = remain_words.pop(0)

        if word == prop:
            ret, r = take_time(remain_words, now)
            if ret:
                return ret, r
            else:
                # time parsing failed
                # backtrack al the way
                return None, words

    # didn't find a specified proposition
    return None, words


def take_duration(words):
    """
    e.g.

    ['two', 'days', 'and', 'five', 'hours', 'from', 'now']

    Returns:
        (datetime.timedelta | None, List[str]): duration and the remainder of words
    """
    # state == 0 -> expecting a number
    # state == 1 -> expecting one of [day, hour, minute, second]
    state = 0
    last_num = 1

    backtrack_position = list(words)
    delta = None

    while words:
        if state == 0:
            if re.match(r'\d+', words[0]):
                last_num = int(words.pop(0))
                state = 1
            elif words[0] in ['a', 'an']:
                words.pop(0)
                last_num = 1
                state = 1
            elif any(words[0].startswith(x) for x in ['day', 'hour', 'minute']):
                last_num = 1
                state = 1
            elif delta is not None and words[0] == 'and':
                words.pop(0)
                continue
            else:
                last_num, words = take_num_words(words)
                if last_num is None:
                    return delta, backtrack_position
                state = 1
        elif state == 1:
            if any(words[0].startswith(x) for x in ['day', 'hour', 'minute', 'second']):
                if words[0].startswith('day'):
                    new_delta = datetime.timedelta(last_num)
                elif words[0].startswith('hour'):
                    new_delta = datetime.timedelta(0, 60 * 60 * last_num)
                elif words[0].startswith('minute'):
                    new_delta = datetime.timedelta(0, 60 * last_num)
                else:
                    new_delta = datetime.timedelta(0, last_num)

                delta = new_delta if delta is None else delta + new_delta
                state = 0
                words.pop(0)
                backtrack_position = list(words)
            else:
                break

    return delta, backtrack_position


def take_for_duration_from_now(words):
    """

    e.g.
    ['for', 'two', 'hours', 'from', 'now']

    Returns:
         (datetime.timedelta, List[str])
    """
    copy_words = list(words)
    duration_words = []

    state = 1

    while copy_words:
        word = copy_words.pop(0)

        if state == 1:
            if word in ['for', 'in']:
                state = 2
        elif state == 2:
            if word == 'from':
                state = 3
            else:
                duration_words.append(word)
        elif state == 3:
            if word == 'now':
                return take_duration(duration_words)[0], copy_words

    return None, words


def take_prop_duration(words, now):
    remain_words = list(words)

    while remain_words:
        word = remain_words.pop(0)
        if word in ['for', 'in', 'by']:
            ret, r = take_duration(remain_words)

            if ret:
                return ret, r
            else:
                break
        elif word in ['until']:
            ret, r = take_time(remain_words, now)

            if ret:
                return ret - now, remain_words[1:]

    return None, words


def take_datetime(words):
    state = 0

    now = datetime.datetime.now()
    date = datetime.datetime.now()
    date = date.replace(microsecond=0)

    time_words = []

    while words:
        word = words.pop(0)

        if state == 0:
            if word == 'at':
                state = 1
        elif state == 1:
            if word == 'am':
                date = date.replace(hour=text2int(time_words[-2:-1]),
                                    minute=text2int(time_words[-1:]), second=0)
                time_words = []
            elif word == 'pm':
                date = date.replace(hour=text2int(time_words[-2:-1]) + 12,
                                    minute=text2int(time_words[-1:]), second=0)
                time_words = []
            elif word == 'o\'clock':
                date = date.replace(hour=text2int(time_words[-1:]),
                                    minute=0, second=0)
                if date < now:
                    date += datetime.timedelta(0, 12 * 60 * 60)
                if date < now:
                    date += datetime.timedelta(0, 12 * 60 * 60)

                time_words = []
            else:
                time_words.append(word)

    if date < now:
        date += datetime.timedelta(1)

    return date
