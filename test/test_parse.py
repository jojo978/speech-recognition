import unittest
import datetime

import voice_command.parse
from voice_command.parse import CommandParserState


class TestParser(unittest.TestCase):
    def test_text2in(self):
        cases = [('twenty three', 23),
                 ('two hundreds and ninety five', 295),
                 ('eleven millions two hundreds thousands and fifty three', 11_200_053)
                 ]

        for a, b in cases:
            self.assertEqual(voice_command.parse.text2int(a.split()), b)

    def test_take_time(self):
        now = datetime.datetime.now().replace(second=0, microsecond=0)
        is_am = now.hour < 12

        cases = [
            (['seven'], ((0, 7, 0), [])),
            (['six', 'thirty', 'the', 'day', 'after', 'tomorrow'],
             ((0, 6, 30), ['the', 'day', 'after', 'tomorrow'])),
            (['three', 'nine'], ((0, 3, 9), [])),
            (['three', 'nine', 'am'], ((1, 3, 9), [])),
            (['three', 'nine', 'pm'], ((1, 15, 9), [])),
            (['at', 'nine', 'o\'clock'], (None, ['at', 'nine', 'o\'clock'])),
            (['6:30', 'a.m.', 'tomorrow'], ((1, 6, 30), ['tomorrow'])),
            (['7', 'a.m.', 'tomorrow'], ((1, 7, 0), ['tomorrow']))
        ]

        p_cases = []
        for a, (ret, r) in cases:
            if ret:
                f, m, h = ret
                x = now.replace(hour=m, minute=h)

                if f == 1:
                    if x < now:
                        x += datetime.timedelta(1, 0)
                if f == 0:
                    if x < now:
                        x += datetime.timedelta(0, 12 * 3600)
                    if x < now:
                        x += datetime.timedelta(0, 12 * 3600)
                p_cases.append((a, (x, r)))
            else:
                p_cases.append((a, (ret, r)))

        for a, b in p_cases:
            self.assertEqual(voice_command.parse.take_time(a), b)

    def test_take_duration(self):
        cases = [
            (['three', 'hours', 'and', 'ten', 'minutes'], (datetime.timedelta(0, 3 * 3600 + 10 * 60), [])),
            (['3', 'hours', '19', 'minutes', 'January'], (datetime.timedelta(0, 3 * 3600 + 19 * 60), ['January']))
        ]

        for a, b in cases:
            self.assertEqual(voice_command.parse.take_duration(a), b)

    def test_take_prop_duration(self):
        now = datetime.datetime.now().replace(second=0, microsecond=0)

        cases = [
            (['in', 'fifteen', 'minutes'], (datetime.timedelta(0, 15 * 60), [])),
            (['for', 'a', 'day', 'and', 'five', 'hours'], (datetime.timedelta(1, 5 * 3600), [])),
            (['by', 'fifteen', 'seconds'], (datetime.timedelta(0, 15), [])),
            (['until', 'seven'], (voice_command.parse.take_time(['seven'])[0] - now, []))
        ]

        for a, b in cases:
            self.assertEqual(voice_command.parse.take_prop_duration(a, now), b)

    def test_parse_start_sleep_command(self):
        cases = [
            ('start sleep', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('begin sleep', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('start a sleep session', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('begin a sleep session', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('begin a new sleep session', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('begin a new sleep session', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('start a new sleep study', (CommandParserState.START_SLEEP_ACCEPTED, None)),
            ('begin a new sleep study', (CommandParserState.START_SLEEP_ACCEPTED, None)),
        ]

        for a, b in cases:
            self.assertEqual(voice_command.parse.take_command(a.split()), b)

    def test_parse_start_focus_command(self):
        cases = [
            ('start focus', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('begin focus', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('start a focus session', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('begin a focus session', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('begin a new focus session', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('begin a new focus session', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('start a new focus study', (CommandParserState.START_FOCUS_ACCEPTED, None)),
            ('begin a new focus study', (CommandParserState.START_FOCUS_ACCEPTED, None)),
        ]

        for a, b in cases:
            self.assertEqual(voice_command.parse.take_command(a.split()), b)

    def test_take_for_from_now(self):
        cases = [
            ('for one hour from now', (0, 3600)),
            ('in two days seven hours five minutes from now', (2, 7 * 3600 + 5 * 60))
        ]

        for a, (days, seconds) in cases:
            self.assertEqual(voice_command.parse.take_for_duration_from_now(a.split()),
                             (datetime.timedelta(days, seconds), []))

    def test_parse_set_alarm_command(self):
        now = datetime.datetime.now()
        now = now.replace(hour=5, minute=30, second=0, microsecond=0)

        cases = [
            ('set alarm six thirty', (0, 6, 30)),
            ('set an alarm for two hours from now', (1, 0, 2 * 3600)),
            ('set an alarm in eight hours thirty minutes from now', (1, 0, 8 * 3600 + 30 * 60)),
            ('set an alarm for nine hours thirty minutes from now', (1, 0, 9 * 3600 + 30 * 60)),
            ('set an alarm for six', (0, 6, 0)),
            ('create an alarm for seven thirty', (0, 7, 30)),
            ('set an alarm for eight', (0, 8, 0)),
            ('set alarm ten', (0, 10, 0)),
            ('make an alarm for eleven', (0, 11, 0)),
            ('set an alarm for 2', (0, 2, 0)),
            ('create an alarm for 7:30', (0, 7, 30)),
            ('create an alarm for 9:15 p.m.', (0, 21, 15))
        ]

        p_cases = []

        for text, (target_type, a, b) in cases:
            if target_type == 0:
                t = now.replace(hour=a, minute=b)
                if t < now:
                    t += datetime.timedelta(0, 12 * 3600)
            else:
                t = now + datetime.timedelta(a, b)

            p_cases.append((text, (CommandParserState.SET_ALARM_ACCEPTED, t)))

        for a, b in p_cases:
            self.assertEqual(voice_command.parse.take_command(a.split(), now=now), b)

    def test_extend_session_command(self):
        cases = [
            ('extend the sleep session by ten minutes',
             (CommandParserState.EXTEND_SLEEP_ACCEPTED, datetime.timedelta(0, 10 * 60))),
            ('extend the sleep session by one hour and fifteen minutes',
             (CommandParserState.EXTEND_SLEEP_ACCEPTED, datetime.timedelta(0, 1 * 3600 + 15 * 60))),
            ('extend the sleep session for two days seven hours and eleven minutes thirty seconds',
             (CommandParserState.EXTEND_SLEEP_ACCEPTED, datetime.timedelta(2, 7 * 3600 + 11 * 60 + 30))),
            ('extend this sleep session for thirty seven seconds',
             (CommandParserState.EXTEND_SLEEP_ACCEPTED, datetime.timedelta(0, 37))),
            ('extend my focus session by fifteen minutes and forty seconds',
             (CommandParserState.EXTEND_FOCUS_ACCEPTED, datetime.timedelta(0, 15 * 60 + 40))),
            ('lengthen focus study by thirty minutes',
             (CommandParserState.EXTEND_FOCUS_ACCEPTED, datetime.timedelta(0, 30 * 60))),
        ]

        for a, b in cases:
            self.assertEqual(voice_command.parse.take_command(a.split()), b)

    def test_tell_health_statistics_command(self):
        cases = [
            'show health statistics',
            'tell me my health statistics',
            'show me my dashboard'
        ]

        for a in cases:
            self.assertEqual(voice_command.parse.take_command(a.split()),
                             (CommandParserState.TELL_HEALTH_STATISTICS_ACCEPTED, None))


if __name__ == '__main__':
    unittest.main()
